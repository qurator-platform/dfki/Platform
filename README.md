# QURATOR Platform

This repository is the main description point for the QURATOR platform and its components. This platform has been designed, implemented and developed during the QURATOR project. The main goal of the platform is to help curators (information processing workers) to easily accomplish complex tasks compmosed of several (sequential or parallel) NLP subtasks. To fulfill this goal, the main components of the platform (described next) are three: DataDukt, the various NLP services and the Graphical User Interface.

[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=for-the-badge)](https://gitlab.com/qurator-platform/dfki/Platform/blob/master/LICENSE)
[![Documentation](https://img.shields.io/badge/Documentation-github-brightgreen.svg?style=for-the-badge)](https://gitlab.com/qurator-platform/dfki/Platform)

## Datadukt (previously known as Workflow Manager)

In the platform, the workflow manager is responsible for the execution of NLP processes. It allows the definition and execution of workflows, enabling the combination of different services integrated in the system. This component is available and documented in [its own repository](https://gitlab.com/qurator-platform/dfki/curationworkflowmanager).


## Available NLP Services
The NLP services that are currently supported and integrated into the QURATOR platform are:

* Named Entity Recognition: it can recognize four types of entities (person, organisation, location,  misc). It is available for German at [NER German](https://gitlab.com/qurator-platform/dfki/srv-bertner-de) and for English at [NER English](https://gitlab.com/qurator-platform/dfki/srv-bertner-en).
* Temporal Expression Analysis for German: We implemented a service for temporal expression analysis, which is based on [Heideltime]() and further improved with multiple domain-specific rules in German. The service is available and documented [here](https://gitlab.com/qurator-platform/dfki/srv-timex-elg).
* Legal Named Entity Recognition: it is a service that recognizes specific named entities in the legal domain. The service is available and documented [here](https://gitlab.com/qurator-platform/dfki/srv-ler).
* Topic Detection: This service assigns content items to one or more human-readable topics. The service is available and documented [here](https://gitlab.com/qurator-platform/dfki/srv-topic-detection).
* Machine Translation: Automatic translation allows to translate text from one language to another. We use the OPUS models, which provide state-of-the-art transformer-based neural machine translation (NMT). We have integrated models for one language pair (English-German) and have tested the models for four additional pairs (English-Spanish, English-French, English-Arabic, English-Russian). The service is available [here](https://gitlab.com/qurator-platform/dfki/srv-translation).
* Summarisation: Our summarisation service offers single-document text summarisation, currently available in English and German. The service is available and documented [here](https://gitlab.com/qurator-platform/dfki/srv-summarization-de).
* Knowledge Extraction and Linking: this service allows the management of knowledge and it is available [here](https://gitlab.com/speaker-projekt/knowledge-management/knowledge-storage-ecosystem).
* Semantic Storytelling: it supports knowledge workers in the (semi)automatic creation of new content out of individual parts, contained in heterogeneous content streams, e.\,g., document collections or social media feeds. The service is available and documented [here](https://github.com/DFKI-NLP/semantic-storytelling/).
* Question Answering: it can be used to find answers to specific questions, unlike information retrieval that returns complete or partial documents containing the answers, in various information sources, such as unstructured documents or knowledge bases. The service is available and documented [here](https://gitlab.com/speaker-projekt/questionanswering/haystack-qa-service).
* Automatic Speech Recognition: it converts a spoken language signal into text and is hosted in the [European Language Grid](https://live.european-language-grid.eu/catalogue/tool-service/487). 
* Text-to-Speech: it converts written text into spoken language and is hosted in the [European Language Grid](https://live.european-language-grid.eu/catalogue/tool-service/4841).


## Demonstration
In this section we are going to include a video screencast of a demonstrator build upon an existing tool [Alleph](http://alleph.com/).

https://gitlab.com/qurator-platform/dfki/Platform/-/blob/main/DemoVideo.mp4

## Installation
In order to install all the componens of the platform and make it run it is only required to have installed [Docker](www.docker.com). All the components (of the platform) have been developed as self-contained docker containers, therefore they do not need specific installation instructions. In case it is needed, these instructions are provided in the individual repositories of the components.

## Usage
The following code shows how to start three components of the QURATOR Platform, and how to process a document through a Dukt (Pipeline or Workflow).

<details><summary>Generate Docker Containers</summary>

:warning: The first thing to do is to obtain the code from the different repositories (see previous list). 

Then, we have to create the docker containers using the following commands:

```
cd Datadukt
docker build -t q_datadukt .
cd ..
cd NER
docker build -t q_ner .
cd ..
cd MT
docker build -t q_mt .
```

</details>

<details><summary>Running Containers</summary>

If all the containers have been generated without errors, now we can run then with the following commands:

```
docker run ...
docker run ...
docker run ...
```
</details>

<details><summary>Processing a document</summary>

Once the containers are up and running, we can process a document through a Dukt using the following command:

```
curl ...
```
</details>

## Support
If you need any support, please contact us at julian.moreno_schneider@dfki.de

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
This platform is licensed under the licenses described in the individual repositories.

## Project status
The development of the QURATOR platform is currently stopped due to the finalization of the QURATOR project. 
